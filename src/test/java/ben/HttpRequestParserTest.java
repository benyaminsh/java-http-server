package ben;

import org.junit.Test;

import static org.junit.Assert.*;

public class HttpRequestParserTest {

    @Test
    public void parseRequest() {

        String rawRequest = "GET / HTTP/1.1\n" +
                "Host: localhost:8080\n" +
                "User-Agent: curl/7.58.0\n" +
                "Accept: */*\n" +
                "\n" +
                "LOL";

        HttpRequest request = HttpRequestParser.parseRequest(rawRequest);

        assertTrue(request.getMethod().equals("GET"));
        assertTrue(request.getTarget().equals("/"));
        assertTrue(request.getHeaders().size() == 3);
        assertTrue(request.getContent().contains("LOL"));
    }
}