package ben;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Date;

public class Main {

    static final int PORT = 8080;

    static final boolean verbose = true;

    public static void main(String[] args) {

        HttpServer server = new HttpServer(PORT);
        server.startServer();
    }
}
