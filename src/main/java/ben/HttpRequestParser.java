package ben;

import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class HttpRequestParser {

    public static HttpRequest parseRequest(String string) {
        String[] linesArray = string.split("\\r?\\n");
        List<String> lines = Arrays.asList(linesArray);

        StringTokenizer tokenizer = new StringTokenizer(lines.get(0));
        String method = tokenizer.nextToken().toUpperCase();
        String target = tokenizer.nextToken();
        String httpVersion = tokenizer.nextToken();

        List<String> headerLines = lines.subList(1, lines.size()).stream()
                .takeWhile(l -> !l.isEmpty()).collect(Collectors.toList());

        // Mapping lines using lambda function to headers
        List<HttpHeader> headers = headerLines.stream().map(l -> {
            String[] splitted = l.split(":", 2);
            String name = splitted[0].trim();
            String value = splitted[1].trim();
            return new HttpHeader(name, value);
        }).collect(Collectors.toList());

        List<String> contentLines = lines.subList(1+headerLines.size(), lines.size());
        String content = contentLines.stream().reduce("", (a,b) -> a + "\n" + b);

        return new HttpRequest(method, target, httpVersion, headers, content);
    }
}
