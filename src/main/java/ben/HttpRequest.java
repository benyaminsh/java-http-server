package ben;

import java.util.List;

public class HttpRequest {
    private String method;
    private String target;
    private String httpVersion;
    private List<HttpHeader> headers;
    private String content;

    public HttpRequest(String method, String target, String httpVersion, List<HttpHeader> headers, String content) {
        this.method = method;
        this.target = target;
        this.httpVersion = httpVersion;
        this.headers = headers;
        this.content = content;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getHttpVersion() {
        return httpVersion;
    }

    public void setHttpVersion(String httpVersion) {
        this.httpVersion = httpVersion;
    }

    public List<HttpHeader> getHeaders() {
        return headers;
    }

    public void setHeaders(List<HttpHeader> headers) {
        this.headers = headers;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "HttpRequest{" +
                "method='" + method + '\'' +
                ", target='" + target + '\'' +
                ", httpVersion='" + httpVersion + '\'' +
                ", headers=" + headers +
                ", content='" + content + '\'' +
                '}';
    }

}
