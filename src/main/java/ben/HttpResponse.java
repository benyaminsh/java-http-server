package ben;

import java.util.ArrayList;
import java.util.List;

public class HttpResponse {
    private String httpVersion;
    private String status;
    private int statusCode;
    private List<HttpHeader> headers;
    private String content;

    public HttpResponse(String httpVersion, String status, int statusCode, List<HttpHeader> headers, String content) {
        this.httpVersion = httpVersion;
        this.status = status;
        this.statusCode = statusCode;
        this.headers = headers;
        this.content = content;
    }

    public List<String> responseLines() {
        List<String> lines = new ArrayList<String>(100);
        lines.add(httpVersion + " " + Integer.toString(statusCode) + " " + status);
        headers.forEach(h -> {
            lines.add(h.getName());
            lines.add(": ");
            lines.add(h.getValue());
        });
        lines.add("");
        lines.add(content);
        return lines;
    }

    public String getHttpVersion() {
        return httpVersion;
    }

    public String getStatus() {
        return status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public List<HttpHeader> getHeaders() {
        return headers;
    }

    public String getContent() {
        return content;
    }

    @Override
    public String toString() {
        return "HttpResponse{" +
                "httpVersion='" + httpVersion + '\'' +
                ", status='" + status + '\'' +
                ", statusCode=" + statusCode +
                ", headers=" + headers +
                ", content='" + content + '\'' +
                '}';
    }
}
