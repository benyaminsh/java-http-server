package ben;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RequestHandler {
    private String path;
    RequestHandler(String path){
        this.path = path;
    }

    private static Path rootDirectory = Paths.get("/home", "ben", "Documents");

    public HttpResponse handleRequest(HttpRequest request) {
        switch (request.getMethod()) {
            case "GET":
                return handleGet(request);
            case "HEAD":
                return handleHEAD(request);
            default:
                return new HttpResponse("HTTP/1.1", "Not Implemented", 501, new ArrayList<>(), "");
        }
        //throw new RuntimeException("");
    }

    private HttpResponse handleGet(HttpRequest request) {
        String target = request.getTarget();

        Paths.get(target);
        Path fullPath = rootDirectory.resolve(target.substring(1));

        System.out.println(fullPath);

        if (!Files.exists(fullPath)) {
            return new HttpResponse("HTTP/1.1", "Not Found", 404, new ArrayList<>(), "");
        } else {
            System.out.println("EXISTS!");
            if (Files.isDirectory(fullPath)) {
                String result = listing(fullPath);
                return new HttpResponse("HTTP/1.1", "OK", 200, List.of(
                        new HttpHeader("Content-Type", "text/plain"),
                        new HttpHeader("Content-Length", Integer.toString(result.length()))
                ), result);
            } else {
                String result = fileContent(fullPath);
                return new HttpResponse("HTTP/1.1", "OK", 200, List.of(
                        new HttpHeader("Content-Type", "text/plain"),
                        new HttpHeader("Content-Length", Integer.toString(result.length()))
                ), result);
            }
        }
    }

    private HttpResponse handleHEAD(HttpRequest request) {
        String target = request.getTarget();

        Paths.get(target);
        Path fullPath = rootDirectory.resolve(target.substring(1));

        System.out.println(fullPath);

        if (!Files.exists(fullPath)) {
            return new HttpResponse("HTTP/1.1", "Not Found", 404, new ArrayList<>(), "");
        } else {
            System.out.println("EXISTS!");
            if (Files.isDirectory(fullPath)) {
                String result = listing(fullPath);
                return new HttpResponse("HTTP/1.1", "OK", 200, List.of(
                        new HttpHeader("Content-Type", "text/plain"),
                        new HttpHeader("Content-Length", Integer.toString(result.length()))
                ), result);
            } else {
                String result = fileContent(fullPath);
                return new HttpResponse("HTTP/1.1", "OK", 200, List.of(
                        new HttpHeader("Content-Type", "text/plain"),
                        new HttpHeader("Content-Length", Integer.toString(result.length()))
                ), result);
            }
        }
    }

    private String fileContent(Path path) {
        try {
            return Files.lines(path).reduce("", (a, b) -> a + "\n" + b);
        } catch (IOException e) {
            e.printStackTrace();
            return "ERROR";
        }
    }

    private String listing(Path path) {
        try {
            return Files.list(path).map(p -> rootDirectory.relativize(p)).map(Object::toString).reduce("", (a, b) -> a.toString() + "\n" +b.toString());
        } catch (IOException e) {
            e.printStackTrace();
            return "ERROR";
        }
    }

}
