package ben;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class Connection implements Runnable {

    Socket clientSocket;

    BufferedWriter out;
    BufferedReader in;

    Connection(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {

        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

           // in.lines().forEach(System.out::println);

            StringBuilder requestSB = new StringBuilder();
            String s;
            while ((s = in.readLine()) != null) {
                requestSB.append(s);
                if (s.isEmpty()) {
                    break;
                }
            }

            String rawRequest = requestSB.toString();
            System.out.println(rawRequest);
            HttpRequest request = HttpRequestParser.parseRequest(rawRequest);

            System.out.println(request);



            RequestHandler requestHandler = new RequestHandler("");
            HttpResponse response = requestHandler.handleRequest(request);

            System.out.println(response);

            String responseString = response.responseLines().stream().reduce("", (a,b) -> a + "\r\n" + b) + "\r\n";

            response.responseLines().forEach(l -> {
                try {
                    out.write(l + "\r\n");
                } catch (IOException e) {
                    System.out.println("Could not write response");
                    e.printStackTrace();
                }
            });

            out.flush();
            out.close();
            in.close();
            clientSocket.close();
        } catch (IOException e) {
            System.out.println("Could not open client conneciton");
            e.printStackTrace();
        }

    }
}
